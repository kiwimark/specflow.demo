﻿#region File Header

// Copyright (c) Orion Health Asia Pacific Limited and the Orion Health Group of companies (2001 - 2015).
// 
// This document is copyright. Except for the purpose of fair reviewing, no part
// of this publication may be reproduced or transmitted in any form or by any
// means, electronic or mechanical, including photocopying, recording, or any
// information storage and retrieval system, without permission in writing from
// the publisher. Infringers of copyright render themselves liable for
// prosecution.

#endregion

namespace View
{
    using System.Windows;

    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
    }
}