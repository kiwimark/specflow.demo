namespace View.ViewModel
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics.CodeAnalysis;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.CommandWpf;
    using Service;
    using Seterlund.CodeGuard;

    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly",
        Justification = "Reviewed. Suppression is OK here.")]
    public class MainViewModel : ViewModelBase
    {
        /// <summary>
        /// The customer search service
        /// </summary>
        private readonly ICustomerSearchService _customerSearchService;

        /// <summary>
        /// The name to search for
        /// </summary>
        private string _name;

        /// <summary>
        /// The results collection of persons
        /// </summary>
        private ObservableCollection<Person> _persons;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        /// <param name="customerSearchService">The customer search service.</param>
        public MainViewModel(ICustomerSearchService customerSearchService)
        {
            Guard.That(customerSearchService).IsNotNull();

            _customerSearchService = customerSearchService;
            Persons = new ObservableCollection<Person>();
            SearchCommand = new RelayCommand(SearchExecute, SearchCanExecute);
        }

        /// <summary>
        /// Gets or sets the search command (The search button uses binding to interact with this relay command)
        /// </summary>
        /// <value>
        /// The search command.
        /// </value>
        public RelayCommand SearchCommand { get; set; }

        /// <summary>
        /// Gets or sets the persons.
        /// </summary>
        /// <value>
        /// The persons.
        /// </value>
        public ObservableCollection<Person> Persons
        {
            get
            {
                return _persons;
            }

            set
            {
                _persons = value;

                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the name to search for.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;

                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Conditions that must be true in order to search
        /// </summary>
        /// <returns><c>true</c> A name has been specified.  Otherwise <c>false</c></returns>
        private bool SearchCanExecute()
        {
            return !string.IsNullOrWhiteSpace(Name);
        }

        /// <summary>
        /// Executes a search
        /// </summary>
        private void SearchExecute()
        {
            // Call the customer search service
            IEnumerable<Person> results = _customerSearchService.Search(Name);

            Persons = new ObservableCollection<Person>(results);
        }
    }
}