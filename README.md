# README #


### What is this repository for? ###

Basic MVVM view with service locator pattern to build SpecFlow tests against for demonstration purposes.

### How do I get set up? ###

1. Open SpecFlow.Demo.sln in Visual Studio
1. Make sure NuGet package manager is set to download missing packages (Visual Studio -> Tools -> Options -> NuGet Package Manger -> Make sure both check boxes are ticked)
1. Build the solution
1. Make sure you can run the view (Right click on the View project -> Debug -> Start new instance)

### Who do I talk to? ###

* Mark

### Links ###

http://www.specflow.org/documentation