﻿namespace Test
{
    using System.Collections.Generic;
    using Moq;
    using NUnit.Framework;
    using Service;
    using TechTalk.SpecFlow;
    using TechTalk.SpecFlow.Assist;
    using View.ViewModel;

    /// <summary>
    /// Search feature steps
    /// </summary>
    [Binding]
    public class SearchFeatureSteps
    {
        /// <summary>
        /// Setup the (fake) database with persons
        /// </summary>
        /// <param name="numberOfPersons">The number of persons</param>
        /// <param name="personName">The person's name</param>
        [Given(@"My customer database contains ""(.*)"" people with the name ""(.*)""")]
        public void GivenMyCustomerDatabaseContainsPeopleWithTheName(int numberOfPersons, string personName)
        {
            // Create a fake/mock customer search service
            Mock<ICustomerSearchService> mockCustomerSearchService = new Mock<ICustomerSearchService>();

            // Build up a list of Peron's for the service to return when called.
            List<Person> persons = new List<Person>();

            for (int i = 0; i < numberOfPersons; i++)
            {
                Person person = new Person(personName, i, 'M');
                persons.Add(person);
            }

            // Setup the customer search service to return our fake/mock persons
            mockCustomerSearchService.Setup(service => service.Search(It.IsAny<string>())).Returns(persons);

            // Add/set the fake/mock customer search service into the ScenarioContext.
            ScenarioContext.Current.Set(mockCustomerSearchService);
        }

        /// <summary>
        /// Enter the person's name into the search screen
        /// </summary>
        /// <param name="personName">The person's name</param>
        [Given(@"I have entered ""(.*)"" into the search screen")]
        public void GivenIHaveEnteredIntoTheSearchScreen(string personName)
        {
            // Get the mock/fake customer search service from the ScenarioContext.
            Mock<ICustomerSearchService> mockCustomerSearchService = ScenarioContext.Current.Get<Mock<ICustomerSearchService>>();

            // Create the view model and inject the fake customer search service
            MainViewModel viewModel = new MainViewModel(mockCustomerSearchService.Object);

            // Set the name property to the input parameter value.
            viewModel.Name = personName;

            // Add/set the view model into the ScenarioContext.
            ScenarioContext.Current.Set(viewModel);
        }

        /// <summary>
        /// Setup the (fake) customer database
        /// </summary>
        /// <param name="table">A table of fake persons to store in the database</param>
        [Given(@"My customer database contains")]
        public void GivenMyCustomerDatabaseContains(Table table)
        {
            // CreateSet is an extension method off of the Table object that will convert the table data to a set of objects.
            // Tip: A default constructor must be provided (one that doesn't have any parameters)
            IEnumerable<Person> persons = table.CreateSet<Person>();

            // Create a fake/mock customer search service.  Note:  We are using the Moq package here (https://github.com/Moq/moq4)
            Mock<ICustomerSearchService> mockCustomerSearchService = new Mock<ICustomerSearchService>();

            // Setup our fake service to return the input table when called.  
            // Note: It.IsAny<string> will return the persons regardless of it's value.  You can specify a name if you wish.
            mockCustomerSearchService.Setup(service => service.Search(It.IsAny<string>())).Returns(persons);

            // Lets store the mock service in the ScenarioContext.
            // ScenarioContext.Current can be used as an IDictionary to store values. 
            // Those values will be retained for the life of the scenario run, 
            // and its contents will be cleared when the next scenario is run.
            ScenarioContext.Current.Set(mockCustomerSearchService);

            // Another way to do this is via keys. E.g
            // ScenarioContext.Current.Add("Some Key", mockCustomerSearchService);
            // Note: You'll need to remember the key and use it to get the object later.
        }

        /// <summary>
        /// Execute search
        /// </summary>
        [When(@"I search")]
        public void WhenISearch()
        {
            // Get the view model from the ScenarioContext
            MainViewModel viewModel = ScenarioContext.Current.Get<MainViewModel>();

            // Execute the search command.  Note: See the .xaml to find the correct command to execute.
            viewModel.SearchCommand.Execute(null);
        }

        /// <summary>
        /// Asserts the correct results are displayed
        /// </summary>
        /// <param name="expectedResultCount">Expected result count</param>
        [Then(@"""(.*)"" results are displayed")]
        public void ThenResultsAreDisplayed(int expectedResultCount)
        {
            // Get the view model from the ScenarioContext
            MainViewModel viewModel = ScenarioContext.Current.Get<MainViewModel>();

            // Check the persons collection is not null.
            Assert.IsNotNull(viewModel.Persons);

            // Check the expected number of person's are correct
            Assert.AreEqual(expectedResultCount, viewModel.Persons.Count);
        }

        /// <summary>
        /// Asserts the results have the expected name
        /// </summary>
        /// <param name="expectedResultName">The expected name</param>
        [Then(@"All results have the name ""(.*)""")]
        public void ThenAllResultsHaveTheName(string expectedResultName)
        {
            // Get the view model from the ScenarioContext
            MainViewModel viewModel = ScenarioContext.Current.Get<MainViewModel>();

            // Check the persons collection is not null.
            Assert.IsNotNull(viewModel.Persons);

            // Check each person has the correct name.
            foreach (var person in viewModel.Persons)
            {
                Assert.AreEqual(expectedResultName, person.Name);
            }
        }
    }
}