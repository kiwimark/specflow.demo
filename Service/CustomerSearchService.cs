﻿namespace Service
{
    using System;
    using System.Collections.Generic;
    using Seterlund.CodeGuard;

    /// <summary>
    /// Customer search service
    /// </summary>
    public class CustomerSearchService : ICustomerSearchService
    {
        /// <summary>
        /// Searches the specified name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns>A list of persons</returns>
        public IEnumerable<Person> Search(string name)
        {
            Guard.That(name).IsNotNullOrWhiteSpace();

            Random random = new Random();
            List<Person> persons = new List<Person>();

            for (int i = 0; i < 10; i++)
            {
                char gender = i % 2 == 0 ? 'F' : 'M';
                int age = random.Next(18, 90);

                Person person = new Person(name, age, gender);

                persons.Add(person);
            }

            return persons;
        }
    }
}